#include "filter.hpp"

Filter::Filter() {
	image_out = new ofstream;
	pixel_out = new Pixel *[getHeight()];
	for(unsigned int i = 0; i <= getHeight(); i++){
		pixel_out[i] = new Pixel [getWidth()];
	}

	while(openToWrite()) {
                char option;
                cout << "Caminho inválido ou não foi possível abrir o arquivo. Tente novamente" << endl;
                cout << "Entre com qualquer tecla para tentar novamente ou '0' para sair" << endl;
                cin.get(option);
                cin.ignore(1, '\n');
                if (option == '0') exit(0);
        }
}

Filter::~Filter(){
	write();
	image_out->close();
//	delete image_out;
	
	/*for(int i = 0; i < getHeight(); i++){
		delete[] pixel_out[i];
	}*/
	//delete[] pixel_out;
}

int Filter::openToWrite(){
	//Interação com o usuário
	string folder;
	string name;
	
	cout << "Você quer especificar o diretório onde vai salvar a nova imagem  ou usar configurações padrão?" << endl;
	cout << "Especifique o diretório (ou aperte ENTER para salvar no diretório padrão \"./saida/imagem.ppm\")" << endl;
	
	getline(cin, folder);
	cout << endl;

	if (!folder.compare("")){
		setPath("./saida/imagem.ppm");
	}

	else {
		if (folder.find_last_of('/')  != folder.size()-1) folder +=  '/'; //Põe barra no final caso não haja
		cout << "Qual o nome da nova imagem?" << endl;
		getline(cin, name);
		cout << endl;

		setPath(folder + name + ".ppm");
	}
	//Abrir o arquivo
	image_out->open(getPath().c_str(), ios::binary);
	if(image_out->fail()) {
                return 1;
	}
	else return 0;
}

void Filter::setPixel(unsigned int i, unsigned int j, string color, unsigned char value){
        if(!color.compare("red")) pixel_out[i][j].red = value;
        else if(!color.compare("green")) pixel_out[i][j].green = value;
        else if(!color.compare("blue")) pixel_out[i][j].blue = value;
        else cout << "setPixel com cor inválida" << endl;
}

void Filter::write(){
	*getImage_out() << getKind()  << endl;
	*getImage_out() << "#Orientação à objetos" << endl;
        *getImage_out() << getWidth() << ' ' << getHeight() << ' ' << "#2º 2016" << endl;
        *getImage_out() << getScale() << endl;

       	for(unsigned int i = 0; i <= getHeight(); i++){
		for(unsigned int j = 0; j < getWidth(); j++){
                        *getImage_out() << (unsigned char)getPixel(i, j, "red", pixel_out);
                        *getImage_out() << (unsigned char)getPixel(i, j, "green", pixel_out);
                        *getImage_out() << (unsigned char)getPixel(i, j, "blue", pixel_out);
                }
        }
	cout << "A imagem foi gravada com sucesso" << endl;
}

ofstream *Filter::getImage_out(){
	return image_out;
}
