#include <iostream>
#include <cstdlib>
#include <sstream>
#include "image.hpp"
#include "filter.hpp"
#include "negativo.hpp"
#include "polarizado.hpp"
#include "pretoBranco.hpp"
#include "media.hpp"

using namespace std;

int main (){
	char option;
	while(true){	
		system("clear");
		cout << "APLICAÇÃO DE FILTROS EM IMAGENS" << endl << endl;

		cout << "Qual filtro deseja aplicar?" << endl << endl;
		cout << "(1) Negativo           (2) Polarizado" << endl;
		cout << "(3) Preto e Branco     (4) Média 3x3" << endl;
		cout << "(5) Média 5x5          (6) Média 7x7" << endl;
		cout << "(7) Média Personalizada" << endl;
		cout << endl;
		cout << "(8) Sair" << endl;

	
		cin.get(option);
		cin.ignore(100, '\n');

		if(option == '1') {
			Negativo imagem;
			imagem.apply();;
		}
        	else if(option == '2') {
                	Polarizado imagem;
			imagem.apply();
        	}
        	else if(option == '3') {
                	PretoBranco imagem;
			imagem.apply();
        	}
        	else if(option == '4') {
                	Media imagem;
			imagem.apply();
        	}
        	else if(option == '5') {
                	Media imagem(5);
			imagem.apply();
        	}
        	else if(option == '6') {
                	Media imagem(7);
			imagem.apply();
        	}
        	
		else if(option == '7'){
			long int size;
			string input = "";
			system("clear");
			
			cout << "Escolha o tamanho da máscara (deve ser um número impar e menor do que 100)" << endl;
			cout << "(Quanto maior a máscara, maior a demora para que o filtro seja aplicado)" << endl;
			do{ //Verifica se está no intervalo correto
						
				do{ //Verifica se é par
				
					while(true){ //Verifica se é um número inteiro 
		            			getline(cin, input);
		            			stringstream myStream(input);
		            			if (myStream >> size)
		            			break;
		            			cout << "Número inválido, tente novamente" << endl;
		    			}
		    		
		    		if (size > 100 or size < 0) cout << "Deve ser um número menor que 100 e maior do que 0" << endl;
		    		
				}while(size > 100 or size < 0);
				
				if (!(size & 1)) cout << "Deve ser um número impar" << endl;
			}while(!(size & 1));

			Media imagem(size);
			imagem.apply();
		}
		
		else if(option == '8') {
			return 0;
		}
		else cout << "Opção inválida" << endl;

		cout << "Deseja continuar aplicando filtros? 's' se sim e qualquer tecla para sair" << endl;

		cin.get(option);
		cin.ignore(100, '\n');

		if(option == 's'){
			//nothing
		}
		else  return 0;
	}
	Negativo imagem;
	imagem.apply();
	return 0;	
}
