#include "negativo.hpp"

void Negativo::apply(){
        for(unsigned int i = 0; i < getHeight(); i++){
                for(unsigned int j = 0; j < getWidth(); j++){
                        setPixel(i, j, "red",  (char)(getScale() - getPixel(i, j, "red" )));
                        setPixel(i, j, "green",  (char)(getScale() - getPixel(i, j, "green" )));
                        setPixel(i, j, "blue",  (char)(getScale() - getPixel(i, j, "blue" )));
                }
        }
}
