#include "pretoBranco.hpp"

void PretoBranco::apply(){
	int grayscale_value;
	
	for(unsigned int i = 0; i < getHeight(); i++){
		for(unsigned int j = 0; j < getWidth(); j++){
           		grayscale_value = (0.289 * getPixel(i, j, "red")) + (0.577 * getPixel(i, j, "green")) + (0.134 * getPixel(i, j, "blue"));
           		setPixel(i, j, "red", grayscale_value);
           		setPixel(i, j, "green", grayscale_value);
			setPixel(i, j, "blue", grayscale_value);
    		}
	}
}
