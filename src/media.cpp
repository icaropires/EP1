#include "media.hpp"

Media::Media(){
	size = 3;
	
	//alocando memória
	mask = new unsigned int *[size];

	for (unsigned int i = 0; i < size; i++){
		mask[i] = new unsigned int [size];
	}
	
	//Atribuindo pesos
	for(unsigned int i = 0; i < size; i++){
		for(unsigned int j = 0; j < size; j++){
			mask[i][j] = 1;
		}
	}	
}

Media::Media(unsigned int size){
	setSize(size);
	//alocando memória
        mask = new unsigned int *[size];

        for (unsigned int i = 0; i < size; i++){
                mask[i] = new unsigned int [size];
        }
    
        //Atribuindo pesos
        for(unsigned int i = 0; i < size; i++){
                for(unsigned int j = 0; j < size; j++){
                        mask[i][j] = 1;
                }
        }  
}

void Media::apply(){
	int limit = (size)/2;
	int x, y, contador = 1;

	for(int i = 0; i < getHeight(); i++){ 
		for(int j = 0; j < getWidth(); j++) {
			value.red = 0;
			value.green = 0;
			value.blue = 0;

			for(x = -limit; x <= limit; x++) {
				for(y = -limit; y <= limit; y++){
					if(i+y >= 0 and i+y < getHeight() and j+x >= 0 and j+x < getWidth()){
						value.red += mask[x+limit][y+limit] * getPixel(i+y, j+x, "red");
						value.green += mask[x+limit][y+limit] * getPixel(i+y, j+x, "green");
						value.blue += mask[x+limit][y+limit] * getPixel(i+y, j+x, "blue");
						contador++;
					}
				}
			}
			
			if(contador > 0){	
				setPixel(i, j, "red", (value.red / contador));
				setPixel(i, j, "green", (value.green / contador));
				setPixel(i, j, "blue", (value.blue / contador));
			}
			contador = 1;
		}
	}
}

unsigned Media::getSize(){
	return size;
}

void Media::setSize(unsigned int size){
	this->size = size;
}
