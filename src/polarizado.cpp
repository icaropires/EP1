#include "polarizado.hpp"

void Polarizado::apply() {
	for(unsigned int i = 0; i < getHeight(); i++){
		for(unsigned int j = 0; j < getWidth(); j++){
       			if((unsigned int)getPixel(i ,j, "red") <  getScale()/2){
           			setPixel(i, j,  "red", 0);
       			}else{
				setPixel(i, j, "red", (unsigned char)getScale());
      			}	

       			if((unsigned int)getPixel(i ,j, "green") < getScale()/2){
				setPixel(i, j, "green", 0);
			}else{
				setPixel(i, j, "green", (unsigned char)getScale());
			}

			if((unsigned int)getPixel(i, j, "blue") < getScale()/2){
				setPixel(i, j, "blue", 0);
			}else{
				setPixel(i, j, "blue", (unsigned char)getScale());
			}
		}
	}
}
