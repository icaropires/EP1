#include "image.hpp"

Image::Image() {
	image = new ifstream;
	while(openImage()) {
		char option;
		cout << "Caminho e/ou arquivo inválido. Tente novamente" << endl;
		cout << "Entre com qualquer tecla para tentar novamente ou '0' para sair" << endl;
		cin.get(option);
		cin.ignore(1, '\n');
		if (option == '0') exit(0);
	}

	kind = findKind();
	width = findWidth();
	height = findHeight();
	scale = findScale();

	//Criando matrix dinamica e alocando memória
	pixel = new Pixel *[height];
	for(i = 0; i <= height; i++) {
		pixel[i] = new Pixel [width];
	}
	
	//Carregando pixels da imagem na matrix
	string trash;

	findScale();
	getline(*image, trash);

	for(i = 0; i < height; i++) {
		for(j = 0; j < width; j++){
			pixel[i][j].red = image->get();
			pixel[i][j].green = image->get();
			pixel[i][j].blue  = image->get();	
		}
	}
}

Image::~Image(){
	//image->close();
	//delete image;
	/*for(i = 1; i < height; i++){
		delete[] pixel[i];
	}*/
	//delete[] pixel;
}

string Image::getPath(){
	return path;
}

void Image::setPath(string path){
	this->path = path;
}

int Image::openImage(){
	//Interação com o usuário
	string folder;
	string name;

	cout << "Especifique o caminho do diretório da imagem (Aperte ENTER para selecionar o padrão \"./doc/)\":" << endl;

	getline(cin, folder);
	cout << endl;
	
	//endereço padrão
	if (!folder.compare("")) folder = "./doc/";

	//adicionar barra no final do arquivo casa não haja
	if (folder.find_last_of('/')  != folder.size()-1) folder +=  '/';
   	
	cout << "Qual imagem? (Não é necessário a extensão)" << endl;
	getline(cin, name);
	cout << endl;
	
	//verificar se o usuário pôs extensão
	if (name.find(".ppm") > name.size()) {
		path = folder + name + ".ppm";
    	}
	else {
		path = folder + name;
	}
	//Abrir a imagem
	image->open(path.c_str(), ios::binary);
	
	//Verificar se a imagem é válida
        if(image->fail()) {
		return 1;
        }
	else return 0;
}


void Image::closeImage(){
	image->close();
}

string Image::findKind(){
	string kind = "#", comment;
	image->seekg(0);

	while(kind[0] == '#'){
        	*image >> kind;
                if (kind[0] == '#') getline(*image, comment);
	}

	return kind;
}

unsigned int Image::findWidth(){
	string str_width = "#", comment;
	unsigned int width = 0;
	unsigned int counter = 1;
	image->seekg(0);

	//Contador para pegar a segunda informação válida, que será a largura
	while(str_width[0] == '#' or counter <= 2){
		*image >> str_width;
		if (str_width[0] == '#') getline(*image, comment);
		else counter++;
        }

	width = atoi(str_width.c_str()); 
	return width;
}

unsigned int Image::findHeight(){
	string str_height = "#", comment;
	unsigned int height = 0;
	unsigned int counter = 1;
	image->seekg(0);

        //Contador para pegar a terceira informação válida, que será a altura
        while(str_height[0] == '#' or counter <= 3){ 
                *image >> str_height;
                if (str_height[0] == '#') getline(*image, comment);
                else counter++;
        }
    
        height = atoi(str_height.c_str());
        return height;
}

unsigned int Image::findScale(){
        string str_scale = "#", comment;
        unsigned int scale = 0;
        unsigned int counter = 1;
	image->seekg(0);
     	
        //Contador para pegar a quarta informação válida, que será a escala de cores
        while(str_scale[0] == '#' or counter <= 4){ 
                *image >> str_scale;
                if (str_scale[0] == '#') getline(*image, comment);
                else counter++;
        }
	
	scale = atoi(str_scale.c_str());
	return scale;
}

void Image::getInfo(){
	cout << "A imagem tem dimensões " << getWidth();
	cout << " X " << getHeight();
	cout << " com escala de " << getScale() << " cores." << endl;
}

string Image::getKind(){
	return kind;
}

ifstream *Image::getImage(){
	return image;
}

unsigned int Image::getWidth(){
	return width;
}

unsigned int Image::getHeight(){
	return height;
}

unsigned int Image::getScale(){
	return scale;
}

Image::Pixel Image::getPixel(unsigned int i, unsigned int j){
	return pixel[i][j];
}

unsigned char Image::getPixel(unsigned int i, unsigned int j, string color){
        if(!color.compare("red")) return pixel[i][j].red;
        else if(!color.compare("green")) return pixel[i][j].green;
        else if(!color.compare("blue")) return pixel[i][j].blue;
        else cout << "getPixel com cor inválida" << endl;
}

unsigned char Image::getPixel(unsigned int i, unsigned int j, string color, Pixel **pixel){         
	if(!color.compare("red")) return pixel[i][j].red;         
	else if(!color.compare("green")) return pixel[i][j].green;         
	else if(!color.compare("blue")) return pixel[i][j].blue;         
	else cout << "getPixel com cor inválida" << endl; 
	}

void Image::setKind(string kind){
	this->kind = kind;
}

void Image::setWidth(unsigned int width){
        this->width = width;
}

void Image::setHeight(unsigned int height){
        this->height = height;
}

void Image::setScale(unsigned int scale){
        this->scale = scale;
}

void Image::setPixel(Pixel pixel){
	this->pixel[i][j] = pixel;
}

void Image::setPixel(unsigned int i, unsigned int j, string color, unsigned char value){
        if(!color.compare("red")) pixel[i][j].red = value;
        else if(!color.compare("green")) pixel[i][j].green = value;
        else if(!color.compare("blue")) pixel[i][j].blue = value;
        else cout << "setPixel com cor inválida" << endl;
}

Image::Pixel **Image::getAllPixels(){
	return pixel;
}
