# EP1 - Orientação à objetos (UnB - Gama)
Este projeto consiste em um programa em C++ capaz de aplicar filtros em imagens de formato `.ppm`.

# Compilar e executar
	Na raiz do diretório execute os seguintes comandos:
	$ make clean
	$ make
	$ make run

# Instruções de uso
	-Ao executar o software será aberto um menu, e basta selecionar as opções desejadas e seguir as dicas do programa.
	-O endereço do diretório da imagem pode ser com relação à pasta raiz do programa ou com relação à raiz do sistema
		ex: /home/user/Desktop/EP1/doc/			#pasta com relação à raiz do sistema
			doc/								#diretório com relação à raiz do programa
	-Cuidado ao escolher o nome e diretório da imagem, pois se houver outra com nome igual, a antiga será substituída sem aviso
	
# Filtros Disponíveis
	Na versão atual, os filtros disponíveis são:
		Negativo;
		Polarizado;
		Preto e Branco;
		Média (qualquer tamanho de máscara que seja ímpar e menor do que 100);
		
# Restrições e recursos
	-Quando o programa estiver aguardando a seleção de uma opção através UM caracter, ele só funcionará corretamente caso digite no máximo 100 caracteres.
	-Ao especificar o diretório, o usuário pode colocar a barra no final do endereço ou deixar que o programa faça isso sozinho.
	-Ao indicar a imagem que deseja editar, é opcional inserir a extensão `.ppm`.
