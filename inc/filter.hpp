#ifndef FILTER_HPP
#define FILTER_HPP

#include <iostream>
#include <string>
#include "image.hpp"

using namespace std;

class Filter : public Image{
	ofstream *image_out;
	string path_out;
	Pixel **pixel_out;
	
public:
	Filter();
	~Filter();

	int openToWrite(); //Retorna 1 em caso de erro e 0 se sucesso
	void write(); //Escreve a matrix ~pixel_saida~
	
	ofstream *getImage_out();
	virtual void apply() = 0;
	
protected:
 	void setPixel(unsigned int i, unsigned int j, string color, unsigned char value);
 	
};
#endif
