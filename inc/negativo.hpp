#ifndef NEGATIVO_HPP
#define NEGATIVO_HPP

#include <iostream>
#include <string>
#include "filter.hpp"

using namespace std;

class Negativo : public Filter{
public:
	Negativo() {};

	void apply();
};

#endif
