#ifndef IMAGE_HPP
#define IMAGE_HPP

#include <string>
#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;

class Image {
public:
	typedef struct pixel {
		unsigned char red;
		unsigned char green;
		unsigned char blue;
	} Pixel;

private:
	//Atributos
	
	ifstream *image;
	string path;
	string kind;
	unsigned int width;
	unsigned int height;
	unsigned int scale;
	Pixel **pixel;
	
	unsigned int i, j;

public:
	Image();
	~Image();
	
	void setPath(string path);

	int openImage(); //retorna 1 caso haja erro
	void closeImage();

	void getInfo();	//imprime as informações sobre a imagem

	string findKind();
	unsigned int findWidth();
	unsigned int findHeight();
	unsigned int findScale();

	string getKind();
	unsigned int getWidth();
	unsigned int getHeight();
	unsigned int getScale();
	ifstream *getImage();


	Pixel getPixel(unsigned int i, unsigned int j);
	unsigned char getPixel(unsigned int i, unsigned int j, string color); //ex: getPixel(100,100, "red")
	unsigned char getPixel(unsigned int i, unsigned int j, string color, Pixel **pixel);
	
protected:
	string getPath();
	void setKind(string kind);
	void setHeight(unsigned int height);
	void setWidth(unsigned int width);
	void setScale(unsigned int scale);

	void setPixel(Pixel pixel);
	void setPixel(unsigned int i, unsigned int j, string color, unsigned char value);
	Pixel **getAllPixels();
};

#endif
