#ifndef MEDIA_HPP
#define MEDIA_HPP
#include "filter.hpp"

class Media : public Filter {
	typedef struct value {
		unsigned int red;
		unsigned int green;
		unsigned int blue;
	} Value;

	unsigned int size;
	unsigned int **mask;
	Value value;

public:
	Media(); //Por padrão a mascara será 3x3
	Media(unsigned int);
	void apply();
	unsigned getSize();
	void setSize(unsigned int size);
};

#endif
